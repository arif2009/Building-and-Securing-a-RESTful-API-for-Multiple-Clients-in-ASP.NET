# Building-and-Securing-a-RESTful-API-for-Multiple-Clients-in-ASP.NET

# Technologies

* [Entity Framework](http://www.asp.net/entity-framework)
* [Json.NET](http://www.newtonsoft.com/json)
* [ASP.NET Web API](http://www.asp.net/web-api)
* [OWIN](http://owin.org)

# Installation

Cloning the repository:

```bash
# HTTPS clone URL
$ git clone https://github.com/arif2009/Building-and-Securing-a-RESTful-API-for-Multiple-Clients-in-ASP.NET.git

# SSH clone URL
$ git clone git@github.com:arif2009/Building-and-Securing-a-RESTful-API-for-Multiple-Clients-in-ASP.NET.git
```

You need to have [Git](https://git-scm.com/)